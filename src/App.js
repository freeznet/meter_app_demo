import React, { Component } from 'react';
import classNames from 'classnames';
import './App.css';

import './main.css';
import {
  Grid
} from '@sketchpixy/rubix';
import Parse from 'parse';
import ParseReact from 'parse-react-eiri'

const ParseComponent = ParseReact.Component(React);

class Hero extends React.Component {
  render() {
    return (
      <div {...this.props}
        className={classNames(this.props.className,
          'homepage-hero')}>
        <Grid fixed>
          {this.props.children}
        </Grid>
      </div>
    );
  }
}

class HeroHeader extends React.Component {
  render() {
    return (
      <div {...this.props}
        className={classNames(this.props.className,
          'homepage-hero-header')}>
        {this.props.children}
      </div>
    );
  }
}

class HeroHeader2 extends React.Component {
  render() {
    return (
      <div {...this.props}
        className={classNames(this.props.className,
          'homepage-hero-header2')}>
        {this.props.children}
      </div>
    );
  }
}

class MeterStatus extends React.Component {
  static propTypes = {
    meter: React.PropTypes.object,
    switch: React.PropTypes.bool.isRequired,
    finish: React.PropTypes.bool.isRequired,
  }

  state = {
    switch: false
  }

  listening = false

  constructor(props) {
    super(props)
    this.sendCmd = this.sendCmd.bind(this)
  }

  _onSubscriptionCreate(object) {
    const reading = object.toPlainObject();
    console.log(reading);
    if (reading.statusType === 'switch') {
      this.setState({
        switch: reading.status
      })
    }
  }

  componentWillUpdate(nextProps, nextState){

    if (nextProps.meter !== undefined && !this.listening && !nextProps.finish) {
      var query = new Parse.Query('SmartMeterStatus').equalTo('smartMeter', nextProps.meter);
      var subscription = query.subscribe();
      this.listening = true
      this.setState({
        switch: nextProps.switch
      })

      subscription.on('open', () => {
        console.log('subscription SmartMeterStatus opened');
      });

      subscription.on('create', this._onSubscriptionCreate.bind(this));

      return true
    }
    if( this.listening )
      return true
    return false
  }

  sendCmd() {
    var SmartMeterCommand = Parse.Object.extend("SmartMeterCommand")
    var Gateway = Parse.Object.extend("Gateway")
    var SmartMeter = Parse.Object.extend("SmartMeter")
    var cmd = new SmartMeterCommand()
    var gateway = new Gateway()
    var smartMeter = new SmartMeter()
    gateway.id = this.props.meter.gateway.objectId
    smartMeter.id = this.props.meter.objectId
    // console.log(this.props.meter)
    cmd.set("statusType", "switch")
    cmd.set("status", !this.state.switch)
    cmd.set("gateway", gateway)
    cmd.set("smartMeter", smartMeter)
    cmd.save()
    this.setState({
        switch: !this.state.switch
      })
  }

  render() {
    var image = '/imgs/app/switch_plug_on.png'
    if (!this.state.switch) {
      image = '/imgs/app/switch_plug_off.png'
    }
    return (
      <div>
        <img src={image} onClick={this.sendCmd} />
      </div>
    )
  }
}

class MeterReading extends React.Component {
  static propTypes = {
    meter: React.PropTypes.object,
    electrical_work: React.PropTypes.number.isRequired,
    voltage: React.PropTypes.number.isRequired,
    current: React.PropTypes.number.isRequired,
    power: React.PropTypes.number.isRequired,
    finish: React.PropTypes.bool.isRequired,
  }

  state = {
    electrical_work: 0.0,
    voltage: 0.0,
    current: 0.0,
    power: 0.0,
  }

  listening = false

  _onSubscriptionCreate(object) {
    const reading = object.toPlainObject();
    // console.log(reading);
    if (reading.identity === 'electrical_work') {
      this.setState({
        electrical_work: reading.reading
      })
    } else if (reading.identity === 'voltage') {
      this.setState({
        voltage: reading.reading
      })
    } else if (reading.identity === 'current') {
      this.setState({
        current: reading.reading
      })
    } else if (reading.identity === 'power') {
      this.setState({
        power: reading.reading
      })
    }
  }

  componentWillUpdate(nextProps, nextState){

    if (nextProps.meter !== undefined && !this.listening && !nextProps.finish) {
      var query = new Parse.Query('SmartMeterReading').equalTo('smartMeter', nextProps.meter);
      var subscription = query.subscribe();
      this.listening = true
      this.setState({
        voltage: nextProps.voltage,
        power: nextProps.power,
        current: nextProps.current,
        electrical_work: nextProps.electrical_work,
      })
      console.log(nextProps)
      console.log(this.state)
      subscription.on('open', () => {
        console.log('subscription SmartMeterReading opened');
      });

      subscription.on('create', this._onSubscriptionCreate.bind(this));

      return true
    }
    if( this.listening )
      return true
    return false
  }

  render() {
    const voltage = this.state.voltage
    const current = this.state.current
    const power = this.state.power
    const electrical_work = this.state.electrical_work
    return (
      <div>
        <p> 电压 : {voltage} V </p>
        <p> 电流 : {current} A </p>
        <p> 功率 : {power} kW </p>
        <p> 电功 : {electrical_work} kWh </p>
      </div>
    )
  }
}

class App extends ParseComponent {
  mixins = [ParseReact.Mixin];
  updating = false;

  state = {
    meter: null
  }

  constructor(props) {
    super(props)
  }

  observe() {
    var SmartMeter = Parse.Object.extend("SmartMeter")
    const meterId = "Kqf0S1QrH0"
    const meterObj = new SmartMeter()
    meterObj.id = meterId
    this.updating = true;
    return {
      meter: (new Parse.Query('SmartMeter')).equalTo('objectId', meterId),
      electrical_work: new Parse.Query('SmartMeterReading').equalTo('smartMeter', meterObj).equalTo('identity', 'electrical_work').descending('date').limit(1),
      voltage: new Parse.Query('SmartMeterReading').equalTo('smartMeter', meterObj).equalTo('identity', 'voltage').descending('date').limit(1),
      current: new Parse.Query('SmartMeterReading').equalTo('smartMeter', meterObj).equalTo('identity', 'current').descending('date').limit(1),
      power: new Parse.Query('SmartMeterReading').equalTo('smartMeter', meterObj).equalTo('identity', 'power').descending('date').limit(1),
      switch: new Parse.Query('SmartMeterStatus').equalTo('smartMeter', meterObj).equalTo('statusType', 'switch').descending('date').limit(1),
    }
  }

  getSwitchStatus(switchObj, currentObj) {
    if (switchObj !== undefined) {
      return switchObj.status
    } else if (currentObj.id !== null) {
      return currentObj.reading !== 0.0
    } else {
      return false
    }
  }

  initReading(o) {
    if (o == null) {
      return { reading: 0.0 }
    } else {
      return o
    }
  }

  componentDidMount() {
        var self = this;
        this.intervalId = setInterval(function () {
            if (self.updating && self.pendingQueries().length == 0) {
                
                self.updating = false;
                self.forceUpdate();
            }
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.intervalId);
    }

  render() {
    var meterObj = this.data.meter[0]
    var switchObj = this.data.switch[0]
    var voltageObj = this.initReading(this.data.voltage[0])
    var currentObj = this.initReading(this.data.current[0])
    var powerObj = this.initReading(this.data.power[0])
    var electricalWorkObj = this.initReading(this.data.electrical_work[0])

    const switchStatus = this.getSwitchStatus(switchObj, currentObj)
    const finish = this.updating
    console.log("finish")
    console.log(finish)
    console.log((this.data.voltage[0]))
    console.log(voltageObj)

    return (
      <div id="homepage-container">
        <Hero>
          <HeroHeader2>{"Smart Meter Demo"}</HeroHeader2>
          <div className='text-center' style={{ marginTop: 25, marginBottom: 25 }}>
            <div>
              <MeterStatus meter={meterObj} switch={switchStatus} finish={finish}/>
            </div>
            <div style={{ marginTop: 25, marginBottom: 25 }}>
              <MeterReading meter={meterObj} voltage={voltageObj.reading} current={currentObj.reading} power={powerObj.reading} electrical_work={electricalWorkObj.reading} finish={finish}/>
            </div>
          </div>

        </Hero>
      </div>
    );
  }
}

export default App;
