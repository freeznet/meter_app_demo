import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import Parse from 'parse';

Parse.initialize("SmartMeterProject", "SmartMeterProjectJavaScriptKey");
Parse.serverURL = "http://192.168.2.112:1337/1";

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
